import os
import requests
import pandas as pd
from packaging import version


def get_env_var(var_name):
    value = os.getenv(var_name)
    if value is None:
        print(f"Missing {var_name} environment variable")
        exit(1)
    return value


CI_PRIVATE_TOKEN = get_env_var("CI_PRIVATE_TOKEN")
CI_PROJECT_ID = get_env_var("CI_PROJECT_ID")

NB_PACKAGES_PER_PAGE = 100
headerToken = {"PRIVATE-TOKEN": CI_PRIVATE_TOKEN}


def fetch_packages():
    packages = []
    hasNext = True
    nextLink = f"https://gitlab.mpcdf.mpg.de/api/v4/projects/{CI_PROJECT_ID}/packages?page=1&per_page={NB_PACKAGES_PER_PAGE}&order_by=created_at&sort=asc"
    while hasNext and nextLink:
        response = requests.get(nextLink, headers=headerToken)
        print(response)

        if response.status_code != 200:
            print("Unable to list Gitlab packages, no cleanup can be done")
            exit(1)

        packages.extend(response.json())
        nextLink = None
        links = response.headers.get("Link", "")
        if 'rel="next"' in links:
            links_parts = links.split(",")
            for part in links_parts:
                if 'rel="next"' in part:
                    nextLink = part[part.find("<") + 1 : part.find(">")]
                    break
        hasNext = nextLink is not None

    return pd.DataFrame(packages)


def find_packages_to_delete(packages: pd.DataFrame):
    grouped = packages.groupby("name")

    packages_to_delete = []

    for _, group in grouped:
        # Convert version strings to version objects for comparison
        group["parsed_version"] = group["version"].apply(version.parse)
        sorted_group = group.sort_values(by="parsed_version", ascending=False)
        # Find the latest non-dev version
        latest_non_dev = sorted_group[
            ~sorted_group["parsed_version"].apply(lambda x: x.is_prerelease)
        ]
        if len(latest_non_dev) == 0:
            continue
        latest_non_dev = latest_non_dev.iloc[0]
        latest_non_dev_version = latest_non_dev["parsed_version"]
        # Add dev versions older than the latest non-dev version to the delete list
        dev_versions_to_delete = sorted_group[
            (sorted_group["parsed_version"] < latest_non_dev_version)
            & (sorted_group["parsed_version"].apply(lambda x: x.is_prerelease))
        ]
        packages_to_delete.extend(dev_versions_to_delete.to_dict("records"))
    return packages_to_delete


def delete_old_packages(packages_to_delete: list[dict]):
    for package_info in packages_to_delete:
        package_id = package_info["id"]
        package_info_label = f"{package_info['name']} - v{package_info['version']}"
        print(f"Deleting package {package_info_label}")
        url = f"https://gitlab.mpcdf.mpg.de/api/v4/projects/{CI_PROJECT_ID}/packages/{package_id}"
        try:
            response = requests.delete(url, headers=headerToken)
            if response.status_code == 204:
                print(f"Package {package_info_label} has been deleted successfully")
            else:
                print(
                    f"/!\\ Unable to delete package {package_info_label}. Status code: {response.status_code}"
                )
        except requests.exceptions.RequestException as e:
            print(f"/!\\ Error: {e}")


def write_new_markdown(packages: pd.DataFrame):
    markdown_file = "packages_table.md"

    with open(markdown_file, "w") as file:
        file.write("| id | name | version | created_at |\n")
        file.write("|----|------|---------|------------|\n")

        for _, row in packages.iterrows():
            file.write(
                f"| {row['id']} | {row['name']} | {row['version']} | {row['created_at']} |\n"
            )


if __name__ == "__main__":
    packages = fetch_packages()
    write_new_markdown(packages)
    packages_to_delete = find_packages_to_delete(packages)
    delete_old_packages(packages_to_delete)

    # fetch packages again and update the table
    packages = fetch_packages()
    write_new_markdown(packages)
